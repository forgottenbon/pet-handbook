
# PET Handbook 
Welcome to the Pinewood Emergency Team! We are an official subsidiary group of Pinewood Builders. We are responsible for taking out fires, healing visitors and cleaning up radioactive waste!

Group: [Pinewood Emergency Team](https://www.roblox.com/groups/2593707/Pinewood-Emergency-Team#!/about) 

## PET Leadership: 

### Owner
- [Diddleshot](https://www.roblox.com/users/390939/profile): Owner of Pinewood Builders 
 
### Current Team Chiefs:

- [AnuCat](https://www.roblox.com/users/69576694/profile) - PET Chief 


## General Reminders
- Make sure to always be respectful to other members, players and visitors especially to those who outrank you, this is all for fun.
- Do not try to evade punishments, It simply doesn’t work like that
- We are not at war with any other groups outside The Mayhem Syndicate 8.
- **Spawnkilling and Mass Random-killing** is absolutely not allowed for anyone, at any time, if you see a user Spawnkilling or Mass Killing, Make sure to call the PIA!
- The PBRF elevator area and Easy Escape area are also considered spawn zones!
- Camping the bottom of the elevator shaft is frowned upon.
- Please follow the Roblox Community Rules 1 at all times, this is still just a game!
- Attacking other on-duty PET members is frowned upon and can lead to a punishment if repeated.
- PBST and TMS must have both a ranktag set to PBST or TMS and PBST or TMS uniforms to be counted as PBST or TMS! (unless they are Special Defense+ or Captain+)
- When patrolling or attending as PET, It’s encouraged that you swap suits every once in a while to make sure you are able to fulfil all Jobs!


## Guidelines
### General Guidelines

PET may not touch core controls that heat or cool the core unless the core has exceeded +/-3000 degrees. E-Coolant may be used to save the core during a meltdown.

PET may not cause disasters while On-Duty.

PET may not carry any PBST or TMS weapons while on duty as PET, however game pass or credit gear such as OP Weapons, the credit rifle/pistol, and rocket launcher are allowed for self defense or saving visitors or security personnel.

PET may NOT be On-Duty and do the job of PBST or TMS.

PET is forbidden to start or spread fires or become mutants/zombies and hurting visitors/allies/PET members.

On-duty PET must have on the uniform and tools (located in PET HQ) and have their rank tag set to PET (!setgroup pet).

During TMS raids, Members can not switch from PET to PBST or TMS.

On-Duty PET are to wear an official PET uniform at all times while patrolling.

PET Members are not allowed to kill Syndicate members in their Cargo Bay Hideout.

### Patrolling Guidelines

Emergency Members are required to report any large fires, radioactive spills or damaged visitors/security members using !call then selecting the “PET” tab and selecting the appropriate reason. Additional notes can be added to your call such as where the disaster or event is located to help responders.

Emergency Members are required to have The Fire Hose, Medical First Aid Kit and Riot Shield along with one of the associated uniforms while on-duty as PET. Tools can be found in the PET Loadout. Tools such as the Fire Extinguisher and Med-Kit are able to be used, it is advised you use the PET Tools as they have better benefits towards fighting disasters and healing users.

Emergency Members are required to put out any fires on sight, heal any visitors or security personnel, No matter what conditions unless told by a Team Specialist+.

If an Emergency Member is to be in the core, only are they allowed to touch core controls when the Core is in Critical Conditions. (+/-3000 Degrees).

Emergency Members can fight anybody who is attempting to meltdown or freezedown the core.

Emergency Members are required to lead out visitors are any cost. Save their lives before your own.

During Chaos Raids, The Mayhem Syndicate is “Kill on Sight”. No other time is anybody on KoS unless authorized by a Team Specialist+.

## Uniforms

When you set your rank tag to PET, you are on-duty for PET and are required to wear an official PET uniform! The PET Headquarters is located past the Transit Area at the Pinewood Computer Core.

You are not allowed to wear a PBST or TMS uniform while on-duty as PET.

PET Team Specialist+ are allowed to wear custom uniforms.

### Uniform links & commands
Links to the Uniforms  
[PET Fire Top](https://www.roblox.com/catalog/256120925/PET-Uniform-Fire)  
[PET Fire Bottom](https://www.roblox.com/catalog/256242195/PET-Fire-pants)  
[PET Medical Top](https://www.roblox.com/catalog/256121653/PET-Uniform-Medi)  
[PET Medical Bottom](https://www.roblox.com/catalog/256242170/PET-Medic-pants)  
[PET Hazmat Top](https://www.roblox.com/catalog/256119389/PET-Uniform-Hazmat)   
[PET Hazmat Bottom](https://www.roblox.com/catalog/256237643/PET-Hazmat-Pants)  

The aliases below are only for people with Donor Commands!
PET Fire Uniform Alias:  
``!importalias !ftuniform !shirt 256120925 & !pants 256242195``  

PET Medical Uniform Alias: 
``!importalias !mtuniform !shirt 256121653 & !pants 256242170``  

PET Hazmat Uniform Alias: 
``!importalias !htuniform !shirt 256119389 & !pants 256237643``  

## Syndicate

While on-duty as an Emergency Respondent, you are expected to help aid PBST when fighting in Chaos or in Meltdowns/Freezedowns.

## PBCC Events

Often when you’re patrolling at the Pinewood Computer Core, a disaster will occur. These disasters do vary damages and seriousness for a few short seconds to minutes.

Disasters are listed as;

- Meltdown
- Freezedown
- Gas Leak
- Earthquake
- Plasma Leak
- Blackout
- Radiation Leak
- Aliens

## Rank Requirements

The Emergency Team has a fairly simple rank structure and requirements for each rank, they are as listed below.

**Owner**
- The rank held by Diddleshot.
- Unobtainable.

**Facilitator**

- Reserved for the Pinewood Facilitators. Currently these are Csdi, Coasterteam and LENEMAR.
- For active PBV Members

**Team Cheif**

- The current Team Chiefs are AtomicMoosh and AnuCat.
- Handpicked by Diddleshot or Facilitators.

**Team Specialist**

- Requires 500 Points, Handpicked by Chiefs, must pass 2 Evaluations, Majority Team Specialist Vote and Team Chief Consensus
- Able to host official events for points.
- Granted Moderator powers in the PBCC !petserver

**Elite Respondent**

- Requires 360 Points

**Experienced Respondent**

- Requires 240 Points

**Trained Respondent**

- Required 72 Points

**Rescuer**

- Given upon joining the Pinewood Emergency Team Group

### Selection Only Rank

**PET Honors Information**

- Honors can be achieved at any rank.

- Honor members get access to a special channel with HRs and other honor members. They also get a special role in the PET Discord and In-Game on their ranktag.

- Team Specialists cannot receive PET Honors.

- PET Honors only last for a total of 3 months. You can get honors again once it expires.

- Honors can be revoked by a Team Chief if you break the PET Handbook several times.

**Raid Response Leader Information**

- Raid Leaders can take attendance for official points at raids.

- Raid Leaders are selected by Team Chiefs

- Raid Leaders can only be given to people that are Trained Respondent - Elite Respondent

- There is no set time limit you can be a Raid Leader.

## Additional Notes

PET HQ is located by the main transit station. From spawn go down the elevator and follow the indicator on the floor to the main transit station door. Once at the door to the transit station, follow the red indicator on the floor to PET HQ. PET HQ contains all team uniforms and equipment.

## FAQ

 1. Are we allowed to carry weapons as PET?  
 You are allowed to carry Credit Weapons or any weapons from a gamepass.

 2. When are trainings hosted?  
 Trainings are hosted at random times, you can check the schedule in game at the PBDSF or run ``k!schedule`` pet in ``#bot-commands`` in the [PET Discord](https://discord.gg/t4KBPkM).

 3. Are we allowed to use the E-Coolant feature?  
Yes, you are allowed to use the E-Coolant if you must save the server. [Refer to this video by **Coasterteam** to see how it works.](https://www.youtube.com/watch?v=nskMAcMYE9M&feature=youtu.be)

::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::
